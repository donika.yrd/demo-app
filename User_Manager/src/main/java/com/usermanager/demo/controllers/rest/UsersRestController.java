package com.usermanager.demo.controllers.rest;

import com.usermanager.demo.exceptions.DuplicateEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.usermanager.demo.models.UserModificationDto;
import com.usermanager.demo.services.UsersService;
import org.springframework.web.bind.annotation.*;
import javax.persistence.EntityNotFoundException;
import com.usermanager.demo.models.UserAccount;
import com.usermanager.demo.models.DtoMapper;
import org.springframework.http.HttpStatus;
import org.omg.CORBA.BAD_PARAM;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/users")
public class UsersRestController {
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public UsersRestController(UsersService usersService, DtoMapper mapper) {
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<UserAccount> getAllUsers() {
        return usersService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserAccount getUserById(@PathVariable int id) {
        try {
            return usersService.getUserById(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public void createUser(@RequestBody @Valid UserModificationDto newUser) {
        try {
            UserAccount account = mapper.fromDtoToModel(newUser);
            usersService.createUser(account);

        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/update")
    public void updateUser(@PathVariable int id,
                           @RequestBody @Valid UserModificationDto updates) {
        UserAccount accountUpdates = mapper.fromDtoToModel(updates);

        try {
            usersService.updateUser(id, accountUpdates);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());

        } catch (DuplicateEntityException d) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, d.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        try {
            usersService.deleteUser(id);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/sort")
    public List<UserAccount> showSortedUsers(@RequestParam(defaultValue = "") String by) {
        try {
            return usersService.sort(by);

        } catch (BAD_PARAM bp) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, bp.getMessage());
        }
    }
}
