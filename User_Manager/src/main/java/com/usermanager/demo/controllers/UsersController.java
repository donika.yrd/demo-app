package com.usermanager.demo.controllers;

import static com.usermanager.demo.utils.Constants.DUPLICATE_EMAIL;
import static com.usermanager.demo.utils.Helper.getErrorMessage;
import com.usermanager.demo.exceptions.DuplicateEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.server.ResponseStatusException;
import com.usermanager.demo.models.UserModificationDto;
import org.springframework.validation.BindingResult;
import com.usermanager.demo.services.UsersService;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.Controller;
import javax.persistence.EntityNotFoundException;
import com.usermanager.demo.models.UserAccount;
import com.usermanager.demo.models.DtoMapper;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.omg.CORBA.BAD_PARAM;
import javax.validation.Valid;
import java.util.List;

@Controller
public class UsersController {
    private UsersService usersService;
    private DtoMapper mapper;

    @Autowired
    public UsersController(UsersService usersService, DtoMapper mapper) {
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping("/users")
    public String getAllUsers(Model model) {
        List<UserAccount> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping("/users/{id}")
    public String getUserById(@PathVariable int id,
                              Model model) {
        try {
            UserAccount account = usersService.getUserById(id);
            model.addAttribute("user", account);
            return "user-account";

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/users/new")
    public String showNewUserForm(Model model) {
        model.addAttribute("newUser", new UserAccount());
        return "create-user";
    }

    @PostMapping("/users/new")
    public String createUser(@Valid @ModelAttribute("newUser") UserModificationDto user,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            String errorMsg = "";
            getErrorMessage(bindingResult, errorMsg);
            model.addAttribute("error", errorMsg);
            return "create-user";
        }

        try {
            UserAccount newUser = mapper.fromDtoToModel(user);
            usersService.createUser(newUser);
            return "redirect:/users";

        } catch (DuplicateEntityException d) {
            model.addAttribute("error", String.format(DUPLICATE_EMAIL, user.getEmail()));
            return showNewUserForm(model);
        }
    }

    @GetMapping("/users/{id}/update")
    public String showUpdateUserForm(@PathVariable int id,
                                     Model model) {
        try {
            UserAccount account = usersService.getUserById(id);
            model.addAttribute("user", account);
            return "update-user";

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/users/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("user") UserModificationDto updates,
                             BindingResult bindingResult,
                             Model model) {
        model.addAttribute("updates", updates);

        if (bindingResult.hasErrors()) {
            String errorMsg = "";
            getErrorMessage(bindingResult, errorMsg);
            model.addAttribute("error", errorMsg);
            return"updare-user";
        }

        try{
            UserAccount updateAccount = mapper.fromDtoToModel(updates);
            usersService.updateUser(id, updateAccount);
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DuplicateEntityException d){
            model.addAttribute("error", String.format(DUPLICATE_EMAIL, updates.getEmail()));
            return "update-user";
        }

        return "redirect:/";
    }

    @GetMapping("/users/{id}/delete")
    public String showDeleteUserForm(@PathVariable int id,
                                     Model model){
        try{
            UserAccount deleteUser = usersService.getUserById(id);
            model.addAttribute("deleteUser", deleteUser);
            return "delete-user";
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/users/{id}/delete")
    public String deleteUser(@PathVariable int id,
                             Model model){
        try{
            usersService.deleteUser(id);
            return "redirect:/users";
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/users/sort")
    public String showSortedUsers(@RequestParam (defaultValue = "") String by,
                                  BindingResult bindingResult,
                                  Model model){
        if (bindingResult.hasErrors()){
            String errorMsg = "";
            getErrorMessage(bindingResult, errorMsg);
            model.addAttribute("error", errorMsg);
            return "users";
        }

        try{
            model.addAttribute("users", usersService.sort(by));
            return "users";
        }catch (BAD_PARAM bp){
            model.addAttribute("error", bp.getMessage());
            return "users";
        }
    }
}
