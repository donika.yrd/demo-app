package com.usermanager.demo.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

import static com.usermanager.demo.utils.Constants.*;

public class UserModificationDto {
    private int id;

    @NotBlank(message = REQUIRED_FIELD_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_NAME_FIELDS, max = MAXIMUM_SIZE_FOR_NAME_FIELDS, message = UNSUITABLE_FIRST_NAME_MASSAGE)
    private String firstName;

    @NotBlank(message = REQUIRED_FIELD_MESSAGE)
    @Size(min = MINIMUM_SIZE_FOR_NAME_FIELDS, max = MAXIMUM_SIZE_FOR_NAME_FIELDS, message = UNSUITABLE_LAST_NAME_MASSAGE)
    private String lastName;

    @Size(min = MINIMUM_SIZE_FOR_EMAIL, max = MAXIMUM_SIZE_FOR_EMAIL, message = UNSUITABLE_EMAIL_MESSAGE)
    private String email;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthDate;

    public UserModificationDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}
