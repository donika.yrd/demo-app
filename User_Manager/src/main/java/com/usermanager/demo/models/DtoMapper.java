package com.usermanager.demo.models;

import org.springframework.beans.factory.annotation.Autowired;
import com.usermanager.demo.services.UsersService;
import org.springframework.stereotype.Component;

@Component
public class DtoMapper {
    private UsersService usersService;

    @Autowired
    public DtoMapper(UsersService usersService) {
        this.usersService = usersService;
    }

    public UserAccount fromDtoToModel(UserModificationDto userDto){
        UserAccount userAccount = new UserAccount();
        userAccount.setId(userDto.getId());
        userAccount.setFirstName(userDto.getFirstName());
        userAccount.setLastName(userDto.getLastName());
        userAccount.setEmail(userDto.getEmail());
        userAccount.setBirthDate(userDto.getBirthDate());

        return userAccount;
    }
}
