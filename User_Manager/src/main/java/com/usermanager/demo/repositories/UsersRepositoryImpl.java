package com.usermanager.demo.repositories;

import static com.usermanager.demo.utils.Helper.updateParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.usermanager.demo.models.UserAccount;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.Session;
import java.util.List;

@Repository
public class UsersRepositoryImpl implements UsersRepository{
    private SessionFactory sessionFactory;

    @Autowired
    public UsersRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserAccount> getAllUsers() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> getUserById(int id) {
       Session session = sessionFactory.getCurrentSession();
       Query<UserAccount> query = session.createQuery(
               "from UserAccount where enabled = true and id = :id", UserAccount.class);
       query.setParameter("id", id);

       return query.list();
    }

    @Override
    public List<UserAccount> getUserByEmail(String email) {
        Session session = sessionFactory.getCurrentSession();
        Query<UserAccount> query = session.createQuery(
                "from UserAccount where enabled = true and email = :email", UserAccount.class);
        query.setParameter("email", email);

        return query.list();
    }

    @Override
    public void createUser(UserAccount newUser) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.save(newUser);
        session.getTransaction().commit();
    }

    @Override
    public void deleteUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        UserAccount disable = getUserById(id).get(0);
        disable.setEnabled(false);

        session.beginTransaction();
        session.update(disable);
        session.getTransaction().commit();
    }

    @Override
    public void updateUser(int id, UserAccount updates) {
        Session session = sessionFactory.getCurrentSession();
        UserAccount updateUser = getUserById(id).get(0);

        updateParameters(updates, updateUser);

        session.beginTransaction();
        session.update(updateUser);
        session.getTransaction().commit();
    }

    @Override
    public List<UserAccount> sortByFirstNameAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by firstName asc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByFirstNameDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by firstName desc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByLastNameAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by lastName asc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByLastNameDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by lastName desc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByEmailAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by email asc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByEmailDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by email desc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByBirthDateAsc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by birthdate asc", UserAccount.class).list();
    }

    @Override
    public List<UserAccount> sortByBirthDateDesc() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery(
                "from UserAccount where enabled = true order by birthDate desc", UserAccount.class).list();
    }
}
