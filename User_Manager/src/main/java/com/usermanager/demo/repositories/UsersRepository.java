package com.usermanager.demo.repositories;

import com.usermanager.demo.models.UserAccount;
import java.util.List;

public interface UsersRepository {
        List<UserAccount> getAllUsers();
        List<UserAccount> getUserById(int id);
        List<UserAccount> getUserByEmail(String email);
        void createUser(UserAccount newUser);
        void deleteUser(int id);
        void updateUser(int id, UserAccount updates);
        List<UserAccount> sortByFirstNameAsc();
        List<UserAccount> sortByFirstNameDesc();
        List<UserAccount> sortByLastNameAsc();
        List<UserAccount> sortByLastNameDesc();
        List<UserAccount> sortByEmailAsc();
        List<UserAccount> sortByEmailDesc();
        List<UserAccount> sortByBirthDateAsc();
        List<UserAccount> sortByBirthDateDesc();
}
