package com.usermanager.demo.utils;

public class Constants {
    public static final String USER_ID_NOT_FOUND = "User with id %d not found.";
    public static final String USER_EMAIL_NOT_FOUND = "User with email '%s' not found.";
    public static final String DUPLICATE_EMAIL = "Account with e-mail: '%s' already exists.";
    public static final String REQUIRED_FIELD_MESSAGE = "Please fill the field.";

    public static final int MINIMUM_SIZE_FOR_NAME_FIELDS = 2;
    public static final int MAXIMUM_SIZE_FOR_NAME_FIELDS = 30;
    public static final int MINIMUM_SIZE_FOR_EMAIL = 8;
    public static final int MAXIMUM_SIZE_FOR_EMAIL = 80;

    public static final String UNSUITABLE_FIRST_NAME_MASSAGE = "First name should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_LAST_NAME_MASSAGE = "Last name should be between {min} and {max} symbols.";
    public static final String UNSUITABLE_EMAIL_MESSAGE = "E-mail should be between {min} and {max} symbols.";

    public static final String INVALID_SORTING_CRITERIA = "Invalid sorting criteria - '%s'.";
}
