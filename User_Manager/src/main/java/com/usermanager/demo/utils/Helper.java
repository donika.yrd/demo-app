package com.usermanager.demo.utils;

import com.usermanager.demo.models.UserAccount;
import org.springframework.validation.BindingResult;

public class Helper {
    public static void updateParameters(UserAccount updates, UserAccount updateUser) {
        updateUser.setBirthDate(updates.getBirthDate());
        updateUser.setFirstName(updates.getFirstName());
        updateUser.setLastName(updates.getLastName());
        updateUser.setEmail(updates.getEmail());
        updateUser.setId(updates.getId());
    }

    public static String getErrorMessage (BindingResult bindingResult, String error){
        if (bindingResult.getFieldError() != null){
            error = bindingResult.getFieldError().getDefaultMessage();
        }
        if (bindingResult.getGlobalError() != null){
            error = bindingResult.getGlobalError().getDefaultMessage();
        }
        return error;
    }
}
