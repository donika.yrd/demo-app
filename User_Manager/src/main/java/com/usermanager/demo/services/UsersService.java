package com.usermanager.demo.services;

import com.usermanager.demo.models.UserAccount;
import java.util.List;

public interface UsersService {
    List<UserAccount> getAllUsers();
    UserAccount getUserById(int id);
    UserAccount getUserByEmail(String email);
    void createUser(UserAccount newUser);
    void deleteUser(int id);
    void updateUser(int id, UserAccount updates);
    List<UserAccount> sort(String orderBy);
}
