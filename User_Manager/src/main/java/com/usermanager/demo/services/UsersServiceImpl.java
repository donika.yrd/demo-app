package com.usermanager.demo.services;

import com.usermanager.demo.exceptions.DuplicateEntityException;
import org.springframework.beans.factory.annotation.Autowired;
import com.usermanager.demo.repositories.UsersRepository;
import static com.usermanager.demo.utils.Constants.*;
import javax.persistence.EntityNotFoundException;
import com.usermanager.demo.models.UserAccount;
import org.springframework.stereotype.Service;
import org.omg.CORBA.BAD_PARAM;
import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<UserAccount> getAllUsers() {
        return usersRepository.getAllUsers();
    }

    @Override
    public UserAccount getUserById(int id) {
        ifIdNotExistThrow(id);
        return usersRepository.getUserById(id).get(0);
    }

    public UserAccount getUserByEmail(String email) {
        return ifEmailNotExistThrow(email);
    }

    @Override
    public void createUser(UserAccount newUser) {
        if (checkIfEmailAlreadyExists(newUser.getEmail())) {
            throw new DuplicateEntityException(String.format(DUPLICATE_EMAIL, newUser.getEmail()));
        }
        usersRepository.createUser(newUser);
    }

    @Override
    public void deleteUser(int id) {
        ifIdNotExistThrow(id);
        usersRepository.deleteUser(id);
    }

    @Override
    public void updateUser(int id, UserAccount updates) {
        ifIdNotExistThrow(id);
        if (checkIfEmailAlreadyExists(updates.getEmail())) {
            throw new DuplicateEntityException(String.format(DUPLICATE_EMAIL, updates.getEmail()));
        }
        usersRepository.updateUser(id, updates);
    }

    @Override
    public List<UserAccount> sort(String orderBy) {
        List<UserAccount> users;

        if (orderBy.isEmpty()) return getAllUsers();

        switch (orderBy.toLowerCase()) {
            case "firstnameasc":
                users = usersRepository.sortByFirstNameAsc();
                return users;
            case "firstnamedesc" :
                users = usersRepository.sortByFirstNameDesc();
                return users;
            case "lastnameasc" :
                users = usersRepository.sortByLastNameAsc();
                return users;
            case "lastnamedesc" :
                users = usersRepository.sortByLastNameDesc();
                return users;
            case "emailasc" :
                users = usersRepository.sortByEmailAsc();
                return users;
            case "emaildesc" :
                users = usersRepository.sortByEmailDesc();
                return users;
            case "birthdateasc" :
                users = usersRepository.sortByBirthDateAsc();
                return users;
            case "birthdatedesc" :
                users = usersRepository.sortByBirthDateDesc();
                return users;
            default: throw new BAD_PARAM(String.format(INVALID_SORTING_CRITERIA, orderBy));
        }
    }

    private void ifIdNotExistThrow(int id) {
        if (usersRepository.getUserById(id).size() == 0) {
            throw new EntityNotFoundException(String.format(USER_ID_NOT_FOUND, id));
        }
    }

    private UserAccount ifEmailNotExistThrow(String email) {
        if (usersRepository.getUserByEmail(email).size() == 0) {
            throw new EntityNotFoundException(String.format(USER_EMAIL_NOT_FOUND, email));
        }
        return usersRepository.getUserByEmail(email).get(0);
    }

    private boolean checkIfEmailAlreadyExists(String email) {
        return usersRepository.getUserByEmail(email).size() == 1;
    }
}
