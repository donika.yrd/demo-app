create database if not exists user_accounts;
use user_accounts;

create table accounts
(
    UserID    int(5)      not null auto_increment,
    Enabled   boolean     not null default true,
    FirstName varchar(30) not null,
    LastName  varchar(30) not null,
    Email     varchar(80),
    BirthDate date,
    primary key (UserID),
    check (length(FirstName) > 1),
    check (length(LastName) > 1),
    check (length(Email) > 7)
);