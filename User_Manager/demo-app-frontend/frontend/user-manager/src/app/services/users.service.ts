import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable()
export class UsersService {
  private usersUrl: string;
 
  constructor(private http: HttpClient,
              private router: Router,
              private route: ActivatedRoute ) { 
    this.usersUrl = 'http://localhost:8080/api/users/';
  }

  public getAllUsers(): Observable<User[]>{
    return this.http.get<User[]>(this.usersUrl);
  }

  public goToUsersList(){
    this.reloadComponent();
    this.router.navigate(['/users']);
  }

  //when deleting entry from the users page router does not reload as it is already on that page
  //thus reloading the component
  reloadComponent() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.router.onSameUrlNavigation = 'reload';
    this.router.navigate(['/same-route']);
  }

  goToEditPage(id: number): void{
    this.router.navigate(['/users/' + id + '/update']);
  }

  public createUser(user: User){
    return this.http.post<User>(this.usersUrl, user);
  }

  public getUserById(id: number): Observable<User>{
    return this.http.get<User>(this.usersUrl + id);
  }

  public updateUser(id: number, updates: User){
    return this.http.put<User>(this.usersUrl + id + '/update', updates).toPromise()
    .then (() => this.goToUsersList());
  }

  public deleteUser(id: number){
    if(confirm("Are you sure you want to delete this account?")){
      return this.http.delete<User>(this.usersUrl + id).toPromise()
      .then (() => this.goToUsersList());
    }
  }
}
