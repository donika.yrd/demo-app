import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {
  @Input() user: User;
  public id: string;

  constructor(private route: ActivatedRoute,
             private router: Router,
             private usersService: UsersService) {
    this.usersService = usersService;
  }

  ngOnInit(){
    this.id = this.route.snapshot.paramMap.get('id');
    this.usersService.getUserById(Number(this.id)).subscribe(data => {
    this.user = data;
    });
  }

  goToEditPage(id: number): void{
    this.usersService.goToEditPage(id);
  }

  // updateUser(id: number, updates: User): void {
  //   this.usersService.updateUser(id, updates).subscribe(data => {
  //     this.user = data;
  //   })
  // }

  deleteUser(id: number): void{
    this.usersService.deleteUser(id);
  }
}

