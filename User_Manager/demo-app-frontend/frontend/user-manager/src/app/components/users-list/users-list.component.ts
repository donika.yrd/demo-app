import {Component, Inject, Injectable, OnInit} from '@angular/core';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  users: User[];
  user: User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private usersService: UsersService) { }

  ngOnInit(): void {
    this.usersService.getAllUsers().subscribe(data => {
      this.users = data;
    });
  }

  goToEditPage(id: number): void{
    this.usersService.goToEditPage(id);
  }

  // editUser(id: number, updates: User){
  //   this.usersService.updateUser(id, updates).subscribe();
  // }

  deleteUser(id: number): void{
    this.usersService.deleteUser(id);
  }
}
