import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-update-user-form',
  templateUrl: './update-user-form.component.html',
  styleUrls: ['./update-user-form.component.scss']
})
export class UpdateUserFormComponent implements OnInit {
  user: User;
  updates: User;
  id: string;

  constructor(private route: ActivatedRoute,
             private router: Router,
             private usersService: UsersService) {
    this.usersService = usersService;
  }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.usersService.getUserById(Number(this.id)).subscribe(data => {
        this.user = data;
    })
  }

  getUpdates(updates) {
      this.updates = {
        "id" : this.user.id,
        "firstName" : updates.firstName,
        "lastName" : updates.lastName,
        "email" : updates.email,
        "birthDate" : updates.birthDate
      }
  }

  updateUser(updates: User){
    this.getUpdates(updates);
    this.usersService.updateUser(this.user.id, this.updates);
  }

  goToHomePage(){
    this.router.navigate(['/']);
  }
}