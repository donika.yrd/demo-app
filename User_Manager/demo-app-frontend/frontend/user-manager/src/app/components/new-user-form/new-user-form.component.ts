import { UsersService } from 'src/app/services/users.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-new-user-form',
  templateUrl: './new-user-form.component.html',
  styleUrls: ['./new-user-form.component.scss']
})
export class NewUserFormComponent {
  user: User;

  constructor(private usersService: UsersService) { 
      this.user = new User();
  }

  onSubmit(){
    this.usersService.createUser(this.user).subscribe(() => this.usersService.goToUsersList());
  }
}
